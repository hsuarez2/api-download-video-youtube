from youtube_dl import YoutubeDL, DownloadError
import getpass

nameUser = getpass.getuser()
routeDownload = f'C:\\Users\\{nameUser}\\Downloads\\NetKin-Songs\\'
#? routeDownload = f'C:\\Users\\{nameUser}\\Desktop\\'

formato = {
    "format" : "bestaudio[ext=m4a]",
    'quiet' : False,
    "postprocessors" : [{
        "key": "FFmpegExtractAudio",
        "preferredcodec"  : "m4a",
        "preferredquality"  : '192'
    }],
    "extractaudio": False,
    "outtmpl"     : routeDownload + "/%(title)s.%(ext)s",
}

def downloadAudioFromVideo(url: str) -> str:
    resp = ""
    if len(url) == 0:
        resp = "No ingreso la url del video."
    elif url[:32] != "https://www.youtube.com/watch?v=":
        resp = "La url ingresada no es valida."
    elif url[32:] == '':
        resp = "La url ingresada no es valida."
    else:
        urlDown = url.strip()[:43]
        try:
            with YoutubeDL(formato) as you:
                you.download([urlDown])
            resp = "Descarga completa."
        except DownloadError as e:
            resp = "Error en la descarga: El url no es correcto."
            print(e)
        except Exception as e:
            resp = "Ups.. ocurrio un problema con la descarga."
            print(e)
    return resp