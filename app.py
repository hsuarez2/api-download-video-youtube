from flask import Flask, request, jsonify
from flask_cors import cross_origin
from services import downloadAudioFromVideo

app = Flask(__name__)

#* Endpoint => http://127.0.0.1:5000/netDownload
@app.route('/netDownload', methods=['POST'])
@cross_origin(origin='*', allow_headers='*')
def downloadVideoYoutube():
    url: str = request.json['url']
    resp = downloadAudioFromVideo(url)
    return jsonify({"msg": resp})

#* Si el nombre de este archivo es igual a main,
#* es decir si se esta ejecutando como archivo principal
if __name__ == "__main__":
    app.run(debug = True, port = 5000)
    # * PARA ENCENDER EL SERVIDOR EN CONSOLA -> python .\app.py